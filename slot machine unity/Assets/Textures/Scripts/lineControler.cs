﻿using UnityEngine;
using System.Collections;

public class lineControler : MonoBehaviour {

	public GameObject master;
	private GUI_Buttons gui;

	public GameObject roll5;
	private rollController rc5;

	public int line_number;
	private bool start;
	public int previous_count;

	public GameObject line1;		
	public GameObject line2;		
	public GameObject line3;		
	public GameObject line4;		
	public GameObject line5;		
	public GameObject line6;		
	public GameObject line7;		
	public GameObject line8;		
	public GameObject line9;		

	private GameObject[] lines;
	
	// Use this for initialization
	void Start () {
		previous_count = 0;
		lines = new GameObject[9]{line1,line2,line3,line4,line5,line6,line7,line8,line9};

		gui = master.GetComponent<GUI_Buttons> ();
		line_number = gui.lineAmount;

		rc5 = roll5.GetComponent<rollController> ();
		start = rc5.start;


		// hide the lines
		for (int i = line_number; i < lines.Length; i++) {
			lines[i].renderer.enabled = false;
		}

	}
	
	// Update is called once per frame
	void Update () {

		line_number = gui.lineAmount;
		start = rc5.start;
		if (!start && line_number != previous_count)
			if(previous_count > line_number){
				for(int i = line_number; i < previous_count; i++)
					lines [i].renderer.enabled = false;
				previous_count = line_number;
			}else{
				for (int i = 0; i < line_number; i++) {
					lines [i].renderer.enabled = true;
				}
				previous_count = line_number;
			}
		else if(start){
			for (int i = 0; i < lines.Length; i++) {
				lines [i].renderer.enabled = false;
			}
			previous_count = 0;
		}else{}

	}
}
