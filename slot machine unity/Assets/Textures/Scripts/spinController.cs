﻿using UnityEngine;
using System.Collections;

public class spinController : MonoBehaviour
{
	public float playbacktime;
	public Sprite sprite0;
	public Sprite sprite1;
	public Sprite sprite2;
	public Sprite sprite3;
	public Sprite sprite4;
	public Sprite sprite5;
	public Sprite sprite6;
	public Sprite sprite7;
	public int spinner0_count;
	public int spinner1_count;
	public int spinner2_count;
	public SpriteRenderer spinner0;
	public SpriteRenderer spinner1;
	public SpriteRenderer spinner2;
	public float timer0;
	public float randomstop0;
	public float randomstop1;
	public float randomstop2;
	private Sprite[] sprite_list;
	// Use this for initialization
	public void spinWheel(int choose){
		if (choose == 0){
			randomstop0 = Random.Range(0.5f,1.5f);
			randomstop1 = Random.Range(0.5f,1.5f);
			randomstop2 = Random.Range(0.5f,1.5f);
			timer0 = 0;
		}else{
			randomstop0 = Random.Range(0.5f,1.5f);
			randomstop1 = Random.Range(0.5f,1.5f);
			randomstop2 = Random.Range(0.5f,1.5f);
			timer0 = 0;
		}
		
	}

	void Start(){
		spinner0_count = 0;
		spinner1_count = 0;
		spinner2_count = 0;
		timer0 = 0;
		playbacktime = 0.0f;
		sprite_list = new Sprite[8];
		sprite_list[0] = sprite0;
		sprite_list[1] = sprite1;
		sprite_list[2] = sprite2;
		sprite_list[3] = sprite3;
		sprite_list[4] = sprite4;
		sprite_list[5] = sprite5;
		sprite_list[6] = sprite6;
		sprite_list[7] = sprite7;


		randomstop0 = Random.Range(0.5f,1.5f);
		randomstop1 = Random.Range(0.5f,1.5f);
		randomstop2 = Random.Range(0.5f,1.5f);
		spinner0.sprite = sprite_list[0];
		spinner1.sprite = sprite_list[1];
		spinner2.sprite = sprite_list[2];

	}
	

	void Update(){
		spinner0.sprite = sprite_list[spinner0_count%8];
		spinner1.sprite = sprite_list[spinner1_count%8];
		spinner2.sprite = sprite_list[spinner2_count%8];
		timer0 += Time.deltaTime;
		randomstop0 -= Time.deltaTime;
		randomstop1 -= Time.deltaTime;
		randomstop2 -= Time.deltaTime;

		if(randomstop0 < 0 && randomstop1 < 0 && randomstop2 < 0){
			Debug.Log(spinner0_count%8 + " " + spinner1_count%8 + " " + spinner2_count%8 );
		}
		if (timer0 > 0.1 && randomstop0 > 0){
			spinner0_count++;
		}
		if (timer0 > 0.1 && randomstop1 > 0){
			spinner1_count++;
		}
		if (timer0 > 0.1 && randomstop2 > 0){
			spinner2_count++;
		}
		if (timer0 > 0.1)
			timer0 = 0;
	}
}