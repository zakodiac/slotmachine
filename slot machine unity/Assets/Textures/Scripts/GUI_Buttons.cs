﻿using UnityEngine;
using System.Collections;
using System.Linq;
using SimpleJSON;


public class GUI_Buttons : MonoBehaviour {
	public float default_width;
	public float default_height;
	public bool user_set;

	public WWW www;
	public WWW deposit;
	public WWW withdraw;
	public bool deposit_called;
	public bool withdraw_called;

	public string user_id;
	public bool inKonami;
	public int konamiPos;
	public string username;
	public bool user_called;

	public rollController[] rc = new rollController[5];
	private int counter;
	public int pay_count;
	public bool paid;
	public bool finished;


	public int[] pay_amounts;
	public int[,] win_paths;

	public int maxLines;

	public int player_wallet;
	public int winnings;
	public bool call_deposit;
	public int[] winning_line;
	public int[] winning_sprite;

	[System.Serializable]
	public class RectInfo {
		public string name = "Start Button";
		public Texture2D icon;
		public float x = 0;
		public float y = 0;
		public float width =0;
		public float height =0;
	}
	public int betAmount;
	public int betTotal;
	public int lineAmount;
	public RectInfo[] buttons;
	public RectInfo[] displays;
	private Vector3 scale;
	public string[] Konami; 
	public AudioSource music;

	// Use this for initialization
	void Start () {
		deposit_called = true;

		withdraw_called = false;
		call_deposit = false;
		inKonami = false;
		Konami =  new string[] {"UpArrow", "UpArrow", "DownArrow", "DownArrow", "LeftArrow", "RightArrow", "LeftArrow", "RightArrow", "B", "A"};
		konamiPos = 0;
		user_set = false;
		user_called = false;

		username = "";

		Application.ExternalEval(
			"var vars = {};"+
            "var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {"+
            "vars[key] = value;"+
            "});" +
			"u.getUnity().SendMessage('Master', 'SetUser', vars['userID']);"+
			"//alert(vars['userID']);");

		paid = true;
		counter = 0;
		winnings = 0;
		pay_count = 0;
		maxLines = 9;
		player_wallet = 10000;
		pay_amounts = new int[] {40, 80, 30, 100, 50, 90, 70 ,60};
		win_paths =  new int[10,5] {{1,1,1,1,1},{0,0,0,0,0},{2,2,2,2,2},{0,1,2,1,0},{2,1,0,1,2},{0,0,1,2,2},{2,2,1,0,0},{1,0,1,2,1},{1,2,1,0,1},{2,2,2,2,2}};
		
		betAmount = 1;
		betTotal = 1;
		lineAmount = 1;

		buttons = new RectInfo[5];
		displays = new RectInfo[5];

		winning_line = new int[10];
		winning_sprite = new int[10];

		buttons[0] = new RectInfo();
		buttons[0].x = 800.0f;
		buttons[0].y = 495.0f;
		buttons[0].width = 120.0f;
		buttons[0].height = 25.0f;

		buttons[1] = new RectInfo();
		buttons[1].name = "Plus Bet Button";
		buttons[1].x = 417.0f;
		buttons[1].y = 495.0f;
		buttons[1].width = 25.0f;
		buttons[1].height = 25.0f;
		buttons[2] = new RectInfo();
		buttons[2].name = "Minus Bet Button";
		buttons[2].x = 290.0f;
		buttons[2].y = 495.0f;
		buttons[2].width = 25.0f;
		buttons[2].height = 25.0f;
		buttons[3] = new RectInfo();
		buttons[3].name = "Plus Lines Button";
		buttons[3].x = 258.0f;
		buttons[3].y = 495.0f;
		buttons[3].width = 25.0f;
		buttons[3].height = 25.0f;
		buttons[4] = new RectInfo();
		buttons[4].name = "Minus Lines Button";
		buttons[4].x = 150.0f;
		buttons[4].y = 495.0f;
		buttons[4].width = 25.0f;
		buttons[4].height = 25.0f;

		displays[0] = new RectInfo();
		displays[0].name = "Bet Value";
		displays[0].x = 327.0f;
		displays[0].y = 496.0f;
		displays[0].width = 120.0f;
		displays[0].height = 25.0f;
		displays[1] = new RectInfo();
		displays[1].name = "Bet Total Value";
		displays[1].x = 482.0f;
		displays[1].y = 496.0f;
		displays[1].width = 120.0f;
		displays[1].height = 25.0f;
		displays[2] = new RectInfo();
		displays[2].name = "Line Count";
		displays[2].x = 185.0f;
		displays[2].y = 496.0f;
		displays[2].width = 120.0f;
		displays[2].height = 25.0f;

		displays[3] = new RectInfo();
		displays[3].name = "Wallet";
		displays[3].x = 40.0f;
		displays[3].y = 496.0f;
		displays[3].width = 120.0f;
		displays[3].height = 25.0f;

		displays[4] = new RectInfo();
		displays[4].name = "Username";
		displays[4].x = 40.0f;
		displays[4].y = 20.0f;
		displays[4].width = 400.0f;
		displays[4].height = 25.0f;

	}

	void SetUser(string param)
	{
		user_id = param;
	}
	void konamiFunction(string key){
		Debug.Log (key);
		if(key == Konami[konamiPos]){
			konamiPos++;
			if(konamiPos == 10){
				player_wallet = 99999999;
				konamiPos = 0;
				username = "Johnny Manziel";
			}
		}else{
			konamiPos = 0;
		}


	}
	void OnGUI()
		
	{
		Event e = Event.current;
		if (e.isKey && Input.anyKeyDown && !inKonami && e.keyCode.ToString()!="None") {
			konamiFunction((e.keyCode).ToString());
		}
		scale.x = Screen.width/default_width; // calculate hor scale
		scale.y = Screen.height/default_height; // calculate vert scale
		scale.z = 1.0f;
		Matrix4x4 svMat = GUI.matrix;
		GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, scale);
	
		GUI.Label(new Rect(displays[0].x, displays[0].y, displays[0].width, displays[0].height),"Bet/Line: "+betAmount);
		GUI.Label(new Rect(displays[1].x, displays[1].y, displays[1].width, displays[1].height),"Total Bet: "+betTotal);
		GUI.Label(new Rect(displays[2].x, displays[2].y, displays[2].width, displays[2].height),"Lines: "+lineAmount);
		GUI.Label(new Rect(displays[3].x, displays[3].y, displays[3].width, displays[3].height),"Bank: "+player_wallet);
		GUI.Label(new Rect(displays[4].x, displays[4].y, displays[4].width, displays[4].height),"User: "+username);
		if (GUI.Button(new Rect(buttons[0].x, buttons[0].y, buttons[0].width, buttons[0].height), "Spin")){	
			Debug.Log("Clicked the button Spin");  
			if(finished && user_set){
				//player_wallet -= betTotal;
				Debug.Log ("Starting withdraw");
				WWWForm withdraw_form = new WWWForm();
				withdraw_form.AddField("userID",user_id);
				withdraw_form.AddField("amount",betTotal);
				withdraw_form.AddField ("gameID","6");
				withdraw = new WWW("http://casino.curtiswendel.me:3000/api/requestFunds", withdraw_form);
				withdraw_called = true;
			}
		}
		if (GUI.Button(new Rect(buttons[1].x, buttons[1].y, buttons[1].width, buttons[1].height), "+") && finished){	
			Debug.Log("Clicked the plus bet button");             
			betAmount++;
			betTotal = betAmount * lineAmount;
			
		}
		if (GUI.Button(new Rect(buttons[2].x, buttons[2].y, buttons[2].width, buttons[2].height), "-") && finished){	
			Debug.Log("Clicked the minus bet button");
			betAmount--;
			if(betAmount <= 0)
				betAmount = 1;
			betTotal = betAmount * lineAmount;
			
		}
		if (GUI.Button(new Rect(buttons[3].x, buttons[3].y, buttons[3].width, buttons[3].height), "+") && finished){	
			Debug.Log("Clicked the plus lines button");
			lineAmount++;
			if(lineAmount > maxLines)
				lineAmount = maxLines;
			betTotal = betAmount * lineAmount;
			
		}
		if (GUI.Button(new Rect(buttons[4].x, buttons[4].y, buttons[4].width, buttons[4].height), "-") && finished){	
			Debug.Log("Clicked the minus lines button");
			lineAmount--;
			if(lineAmount < 1)
				lineAmount = 1;

			betTotal = betAmount * lineAmount;
		}
		GUI.matrix = svMat;
	}
	// Update is called once per frame
	void check_win(int lines, int bet){
		int j = 0;
		int previous_slot = -1;
		float temp_winning = 0.0f;
		for(int z = 0; z < lines; z++){
			Debug.Log("Running check for line: "+z);
			pay_count = 0;
			counter = 0;
			for(int i = 0; i < 5; i++){
				j = win_paths[z, i];
				Debug.Log ("Checking sprite " + j.ToString() + " in roller: " + i.ToString() + "And sprite is: " + rc[i].final_position[j].ToString()); 

				if(i == 0){
					previous_slot = rc[i].final_position[j];
					counter = 1;
				}else if( previous_slot == rc[i].final_position[j] ){
					counter++;
					if(counter > pay_count)
						pay_count = counter;
					if(counter == pay_count)
						winning_sprite[z] = previous_slot;
					previous_slot = rc[i].final_position[j];
				}else{
					//pay_count = Mathf.Max(counter, pay_count);
					if(counter > pay_count)
						pay_count = counter;
					if(counter == pay_count)
						winning_sprite[z] = previous_slot;
					counter = 1;
					previous_slot = rc[i].final_position[j];
				}

			}

			winning_line[z] = pay_count;
			if(winning_line[z] >= 3){
				temp_winning = (Mathf.Pow(10.0f,(winning_line[z]%3)) ) * ((float)bet/10.0f) * (float)pay_amounts[winning_sprite[z]];
				player_wallet += (int)Mathf.Floor(temp_winning);
				winnings += (int)Mathf.Floor(temp_winning);
				Debug.Log("Paid for winnings for sprite: "+ winning_sprite[z] + "And won a total of: " + temp_winning );
			}
		}

	}

	void Update () {

		rc = gameObject.GetComponentsInChildren<rollController>().OrderBy(go => go.name).ToArray();
		if(!user_set && !user_called && user_id != ""){
			www = new WWW("http://casino.curtiswendel.me:3000/api/getUser/"+user_id);
			user_called = true;
		}
		if(!user_set && user_called && www.isDone){
			string temp = www.text;
			var N = JSON.Parse(temp);
			username = N["screenName"];
			player_wallet = int.Parse(N["balance"]);
			user_set = true;
		}

		finished = rc[4].finished;

		if(finished && !paid){
			music.Stop();
			deposit_called = false;
			check_win (lineAmount, betAmount);
			paid = true;
			call_deposit = true;
		}
		if(call_deposit && user_called && paid && !deposit_called){
			WWWForm deposit_form = new WWWForm();

			deposit_form.AddField("userID",user_id);
			if(winnings == 0)
				winnings = -1;
			deposit_form.AddField("amount",winnings);
			deposit_form.AddField ("gameID","6");
			deposit = new WWW("http://casino.curtiswendel.me:3000/api/addTransaction", deposit_form);
			winnings = 0;
			deposit_called = true;
		}
		if(call_deposit && deposit_called && deposit.isDone){
			deposit_called = false;
			string temp = deposit.text;
			Debug.Log(temp);
			var N = JSON.Parse(temp);
			player_wallet = int.Parse(N["balance"]);
			call_deposit = false;

		}
		if(withdraw_called && withdraw.isDone){
			withdraw_called = false;
			string temp = withdraw.text;
			Debug.Log ("Test");
			Debug.Log(temp);
			SimpleJSON.JSONNode N = JSON.Parse(temp);
			string success = N["success"];
			string error = N["error"];

			Debug.Log (success);
			Debug.Log (error);

			if(error == null && success != null){
				player_wallet -= betTotal;
				if(player_wallet <= 0){
					Debug.Log ("You are broke you can not play");
				}else{
					
					rc[0].start = true;
					rc[1].start = true;
					rc[2].start = true;
					rc[3].start = true;
					rc[4].start = true;
					
					music.Play();
					
				}
			
			}else
				Debug.Log(N["error"]);
			}


		if(!finished)
			paid = false;
	}
}
