﻿using UnityEngine;
using System.Collections;

public class rollController : MonoBehaviour {


//	public float speed_max;
	public float y_starting_position;
	public float y_ending_position;
	public bool start = false;
	public string test;
	public float timer;
	public bool finished;
	private int spriteNumber;
	private float velocity;
	private float speed_param;

	private float timer_original;

	public GameObject sprite0;
	public GameObject sprite1;
	public GameObject sprite2;
	public GameObject sprite3;
	public GameObject sprite4;
	public GameObject sprite5;
	public GameObject sprite6;
	public GameObject sprite7;

	private GameObject[] sprites;
	
	public int[] final_position;

	public AudioSource stop;

	// Use this for initialization
	void Start () {
		spriteNumber = 8;
		speed_param = 10;
		start = false;
		finished = true;
		velocity = (-0.1f) * speed_param;
		timer_original = timer;

		sprites = new GameObject[spriteNumber];
		initializeSprites ();

		final_position = new int[3];

	}

	// initialize the sprite array to make sure they are correct
	void initializeSprites(){

		GameObject[] temp_sprites = new GameObject[8];
		temp_sprites [0] = sprite0;
		temp_sprites [1] = sprite1;
		temp_sprites [2] = sprite2;
		temp_sprites [3] = sprite3;
		temp_sprites [4] = sprite4;
		temp_sprites [5] = sprite5;
		temp_sprites [6] = sprite6;
		temp_sprites [7] = sprite7;


		int k = 0; 
		for (int yPos = 6; yPos >= -8; yPos-=2){
			for (int i = 0; i < 8; i++){
				if (temp_sprites[i].transform.position.y == yPos){
					sprites[k] = temp_sprites[i];
					break;
				}
			}
			k++;
		}

	}
	
	// Update is called once per frame
	void Update () {


		// if count down is done, stop spinning
		if (start) {
			timer -= Time.deltaTime;
			if (timer > 0) {
				spin ();
			}
			else {
				//spin the wheel to make them fit the screen

				start = false;
				timer = timer_original;

				stop.Play();

				spin_finish();

			}
		}
	}

	// 2 0 -2
	void final_position_check(){

		for (int i = 0; i < sprites.Length; i++) {
			if (sprites[i].transform.position.y == 2)
				final_position[0] = sprites[i].name[8] - 48;
			else if (sprites[i].transform.position.y == 0)
				final_position[1] = sprites[i].name[8] - 48;
			else if (sprites[i].transform.position.y == -2)
				final_position[2] = sprites[i].name[8] - 48;
		}
	}

	// make sure that the each sprite's position are correct at the end of the spin
	// 6 4 2 0 -2 -4 -6 -8
	void spin_finish(){

		float y = sprites [0].transform.position.y;
		Vector3 newPosition = sprites [0].transform.position;
		if (y >= 6) {
			newPosition.y = 6;
		}
		else if (y < 6 && y >= 4){
			newPosition.y = 4;
		}
		else if (y < 4 && y >= 2){
			newPosition.y = 2;
		}
		else if (y < 2 && y >= 0){
			newPosition.y = 0;
		}
		else if (y < 0 && y >= -2){
			newPosition.y = -2;
		}
		else if (y < -2 && y >= -4){
			newPosition.y = -4;
		}
		else if (y < -4 && y >= -6){
			newPosition.y = -6;
		}
		else{
			newPosition.y = -8;
		}

		sprites [0].transform.position = newPosition;


		y = newPosition.y;
		for (int i = 1; i < sprites.Length; i++) {
			y -= 2;
			if (y < -8){
				y = 6;
			}

			newPosition = sprites [i].transform.position;
			newPosition.y = y;
			sprites [i].transform.position = newPosition;

		}

		final_position_check();
		finished = true;
	}

	// spin the sprites
	void spin(){
		finished = false;
		for (int i = 0; i < spriteNumber; i++) {
			if (sprites [i].transform.position.y < y_ending_position) {
					sprites [i].transform.position = new Vector3 (sprites [i].transform.position.x, y_starting_position, 0);
					Debug.Log ("reached!");
			}

			// moving down
			sprites [i].transform.Translate (0, velocity, 0, Space.World);
		}
	}
	
}
